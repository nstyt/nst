#define _CRT_SECURE_NO_WARNINGS

#include "hash.h"

#include <stdio.h>

#define LIMIT 256
#define TAB "\t"
#define END_STRING '\0'
#define FALSE 0
#define TRUE 1
#define EMPTY_STRING ""

void fill_HashList(HashList *hash_list, const char *file_name)
{
	FILE *f = fopen(file_name, "r");
	char tmp[LIMIT];
	char *tmp_key;
	while (fgets(tmp, LIMIT, f) != NULL)
	{
		char *str = strstr(tmp, TAB);
		tmp_key = tmp;
		*str = END_STRING;
		str++;
		add_element_HashList(hash_list, tmp_key, str);
	}
}

int hash_func(const char *value)
{
	char *iter = value;
	int res = 0;
	for (iter = value; *iter != END_STRING; iter++)
	{
		res += *iter;
	}
	return res;
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		printf("%s\n", "Wrong count of args!");
	}
	HashList hash_list;
	init_HashList(&hash_list);
	set_hash_func_HashList(&hash_list, hash_func);
	fill_HashList(&hash_list, argv[1]);
	char tmp[LIMIT];
	int isExit = FALSE;
	while (gets(tmp))
	{
		if (strcmp(tmp, EMPTY_STRING) == 0)
		{
			if (isExit)
				break;
			else
				isExit = TRUE;
		}
		else
		{
			printf("%s\n", get_element_HashList(&hash_list, tmp));
			isExit = FALSE;
		}
	}
	delete_HashList(&hash_list);
	return 0;
}