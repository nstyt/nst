#ifndef LIST_H
#define LIST_H

typedef struct Element 
{
	char *value;
	char *key;
	struct Element *next;
} Element;

typedef struct List
{
	Element *head;
} List;

void init_list(List *list);
void add_element_list(List *list, const char *key, const char *value);
const char *find_value_list(const List *list, const char *key);
void show_list(const List *list);
void delete_list(List *list);
int count_list(const List *list);
int get_value_by_num_list(const List *list, int num);

#endif