#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

void main() {
	double N, mantissa2;
	long long int znak, exp,mantissa;
	double A, *d;
	int B;
	for (;;) {
		scanf("%lf", &N);
		long long int *p = &N;
		znak = *p >> 63;
		if (znak == 0)
		{
			printf("znak= +\n");
		}
		else
		{
			printf("znak= -\n");
		}
		exp = ((*p >> 52) & 0x7ff) - 1022;
		printf("exponenta= %lli\n", exp);
		mantissa = (*p & 0xfffffffffffff) | 0x3fe0000000000000;
		d= &mantissa;
		mantissa2 = *d;
		printf("mantissa= %lf\n", mantissa2);

		A = frexp(N, &B);
		printf("\nfrexp: %lf     %i\n", A, B);

	}
}