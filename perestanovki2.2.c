#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void buildinvers(int* tinvers, int N) {
	int i = 0;

	tinvers[i]++;	
	for (i = 0; i < N; i++) {
		if (tinvers[i] == N-i)
		{	                          
		tinvers[i + 1]++;	
		tinvers[i] = 0;	
		}
	}
}

void building(int* tinvers, int* perest, int N) {
	int l = 0;
	int pos = 0;	
	int num = 1;

	memset(perest, 0, sizeof(int) * N);	
	for (l = 0; l < N; l++) {
		int i = 0;
		for (i = 0; i < N; i++) {
			if (perest[i] == 0) {	
				if (pos == tinvers[l]) {	
					perest[i] = num;
					break;
				}
				else {
					pos++;
				}
			}
		}
		num++;
		pos = 0;
	}
}

void print(int N, int* perest) {		
	int j = 0;

	for (j = 0; j < N; j++) {
		printf("%d ", perest[j]);
	}
	printf("\n");
}

int main(int argc, char* argv[]) {
	int N = 0;
	int i = 0;
	int j = 0;
	int check = 0;

	if (argc!= 2) {	
		scanf("%d", &N);
	}
	else {
		N = atoi(argv[1]);
	    }

	if (N < 0) {
		printf ("Wrong length.It should be positive\n");
		return 1;
	}

	int* tinvers = (int*)calloc(sizeof(int), N);	
	int* perest = (int*)calloc(sizeof(int), N);	

	building(tinvers,perest, N);	
	if (N != 1) {
		print(N, perest);
	}

	while (N > check) {
		buildinvers(tinvers, N);	
		building(tinvers,perest, N);	
		print(N,perest);	

		check = 0;
		for (j = 0; j < N; j++) {	
			if (N - j - 1 == tinvers[j]) {
				check++;
			}
		}
	}

	free(tinvers);
	free(perest);
	return 1;
}