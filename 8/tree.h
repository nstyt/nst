#ifndef TREE_H
#define TREE_H

#include "list.h"

#include <stdlib.h>

typedef struct Node
{
	int value;
	struct Node *left;
	struct Node *right;
} Node;

Node *init_tree(const List *list, int start, int end);
void print_tree(const Node *root);
void delete_tree(Node *root);

#endif