#define _CRT_SECURE_NO_WARNINGS

#include "list.h"
#include "tree.h"
#include <stdio.h>


int comparator(int a, int b)
{
	return (a - b) > 0;
}

int main(int argc, char **argv)
{
//	freopen("myfile.txt", "w", stdout);
	if(argc != 2)
	{
		printf("%s\n", "Wrong count of args!");
	}
	FILE *f = fopen(argv[1], "r");
	int value = 0;
	List *list = init_list();
	while(fscanf(f, "%d", &value) != EOF)
	{
		add_element_list(list, value);
	}
	sort_list(list, comparator);
	show_list(list);
	Node *root = init_tree(list, 0, count_list(list) - 1);
	delete_list(list);
	print_tree(root);
	delete_tree(root);
	return 0;
}