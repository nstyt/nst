#ifndef LIST_H
#define LIST_H

typedef struct Element 
{
	int value;
	struct Element *next;
} Element;

typedef struct List
{
	Element *head;
} List;

List *init_list();
void add_element_list(List *list, int value);
void sort_list(List *list, int (*comp)(int first, int second));
void show_list(const List *list);
void delete_list(List *list);
int count_list(const List *list);
int get_value_by_num_list(const List *list, int num);

#endif