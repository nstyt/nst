#define _CRT_SECURE_NO_WARNINGS
#include "tree.h"
#include <stdio.h>
#include <string.h>
#define SPACE_COUNT 80

Node *init_tree(const List *list, int start, int end)
{
	Node *node = NULL;
	int root_num = (start + end) / 2;

	node = (Node*)calloc(1, sizeof(Node));
	node->value = get_value_by_num_list(list, root_num);

	if (end == start)
	{
		node->left = NULL;
		node->right = NULL;
	}
	else if (end - start == 1)
	{
		node->left = NULL;
		node->right = init_tree(list, end, end);
	}
	else
	{
		node->left = init_tree(list, start, root_num - 1);
		node->right = init_tree(list, root_num + 1, end);
	}
	return node;
}

static void print_space(int count)
{
	int i = 0;
	for (i = 0; i < count; i++)
		printf(" ");
}

static void print_num(int count, int num)
{
	char *str[SPACE_COUNT];
	int size=sprintf(str, "%u", num);
	for (int i = size; i < count; i++)
		strcat(str, " ");
	printf("%s", str);
}

void print_layer(const Node *node, int deep, int need_deep)
{	
	if(deep == need_deep)
	{
		int i = 0;
		if (node)
			print_num(SPACE_COUNT / (1<<deep), node->value);
		else
			print_space(SPACE_COUNT / (1 << deep));
	}
	else
	{
		print_layer(node->left, deep + 1, need_deep);
		print_layer(node->right, deep + 1, need_deep);
	}
}

void print_tree(const Node *root)
{
	const Node *node = root;
	int height = 1;
	int i = 0;
	while (node->right != NULL)
	{
		node = node->right;
		height++;
	}
	for(i = 0; i < height; i++)
	{
		print_space(SPACE_COUNT / (1 << (i+1)));
		print_layer(root, 0, i);
		printf("\n");
	}
}

void delete_tree(Node *root)
{
	Node *left_tmp = root->left;
	Node *right_tmp = root->right;
	free(root);
	if (left_tmp)
		delete_tree(left_tmp);
	if (right_tmp)
		delete_tree(right_tmp);
}

