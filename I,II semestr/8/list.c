#include "list.h"

#include <stdlib.h>
#include <stdio.h>

List *init_list()
{
	List *list = (List*)calloc(1,sizeof(List));
	list->head = NULL;
	return list;
}


void add_element_list(List *list, int value)
{
	Element *element = (Element*)calloc(1,sizeof(Element));
	element->next = list->head;
	element->value = value;
	list->head = element;
}

void sort_list(List *list, int (*comp)(int first, int second))
{
	Element *iter = list->head;
	Element *prev = NULL;
	int count = count_list(list);
	int i = 0;
	int j = 0;
	for(i = count - 1; i > 0; i--)
	{
		iter = list->head;
		prev = NULL;
		for(j = 0; j < i; j++)
		{
			if(comp(iter->value, iter->next->value))
			{
				Element *tmp = iter->next;
				iter->next = tmp->next;
				tmp->next = iter;
				if(prev)
					prev->next = tmp;
				else
					list->head = tmp;
				iter = tmp;
			}
			prev = iter;
			iter = iter->next;
		}
	}
}

int count_list(const List *list)
{
	Element *iter = list->head;
	int count = 0;
	while(iter != NULL)
	{
		iter = iter->next;
		count++;
	}
	return count;
}

void show_list(const List *list)
{
	Element *iter = list->head;	
	for(iter = list->head; iter != NULL; iter = iter->next)
	{
		printf("%d, ", iter->value);
	}
	printf("\n");
}

void delete_list(List *list)
{
	Element *iter = list->head;	
	while(iter != NULL)
	{
		Element *tmp = iter->next;
		free(iter);
		iter = tmp;
	}
	free(list);
}

int get_value_by_num_list(const List *list, int num)
{
	Element *iter = list->head;	
	int i = 0;
	for(iter = list->head, i = 0; i < num; iter = iter->next, i++);
	return iter->value;
}

