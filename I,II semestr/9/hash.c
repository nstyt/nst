#include "hash.h"

void init_HashList(HashList *hash_list)
{
	int i = 0;
	for (i = 0; i < N; i++)
	{
		init_list(&hash_list->array[i]);
	}
}

void set_hash_func_HashList(HashList *hash_list, int(*p)(const char *str))
{
	hash_list->hash_func = p;
}

void add_element_HashList(HashList *hash_list, const char *key, const char *value)
{
	int hash = hash_list->hash_func(key) % N;
	add_element_list(&hash_list->array[hash], key, value);
}

const char *get_element_HashList(const HashList *hash_list, const char *key)
{
	int hash = hash_list->hash_func(key) % N;
	return find_value_list(&hash_list->array[hash], key);
}

void delete_HashList(const HashList *hash_list)
{
	int i = 0;
	for (i = 0; i < N; i++)
	{
		delete_list(&hash_list->array[i]);
	}
}
