#ifndef HASH_H
#define HASH_H

#include "list.h"

#define N 1000

typedef struct HashList
{
	List array[N];
	int(*hash_func)(const char *str);
} HashList;

void init_HashList(HashList *hash_list);
void set_hash_func_HashList(HashList *hash_list, int(*p)(const char *str));
void add_element_HashList(HashList *hash_list, const char *key, const char *value);
const char *get_element_HashList(const HashList *hash_list, const char *key);
void delete_HashList(const HashList *hash_list);

#endif