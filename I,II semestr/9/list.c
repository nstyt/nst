#define _CRT_SECURE_NO_WARNINGS

#include "list.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void init_list(List *list)
{
	list->head = NULL;
}


void add_element_list(List *list, const char *key, const char *value)
{
	Element *element = (Element*)calloc(1,sizeof(Element));
	element->next = list->head;
	element->key = (char *)calloc(1, strlen(key) + 1);
	strcpy(element->key, key);
	element->value = (char *)calloc(1, strlen(value) + 1);
	strcpy(element->value, value);
	list->head = element;
}

int count_list(const List *list)
{
	Element *iter = list->head;
	int count = 0;
	while(iter != NULL)
	{
		iter = iter->next;
		count++;
	}
	return count;
}

const char *find_value_list(const List *list, const char *key)
{
	Element *iter = list->head;
	for (iter = list->head; iter != NULL; iter = iter->next)
	{
		if (strcmp(iter->key, key) == 0)
			return iter->value;
	}
	return NULL;
}

void show_list(const List *list)
{
	Element *iter = list->head;	
	for(iter = list->head; iter != NULL; iter = iter->next)
	{
		printf("%d, ", iter->value);
	}
	printf("\n");
}

void delete_list(List *list)
{
	Element *iter = list->head;	
	while(iter != NULL)
	{
		Element *tmp = iter->next;
		free(iter->key);
		free(iter->value);
		free(iter);
		iter = tmp;
	}
}

int get_value_by_num_list(const List *list, int num)
{
	Element *iter = list->head;	
	int i = 0;
	for(iter = list->head, i = 0; i < num; iter = iter->next, i++);
	return iter->value;
}

