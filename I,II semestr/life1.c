#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

#define DEAD (0)
#define LIVE (1)

int check(int *field1, int x, int y, const int width, const int height) {

	int x1 = 0;
	int y1 = 0;
	int k = 0;

	for (int i = -1; i <= 1; i++) {
		for (int j = -1; j <= 1; j++) {
			x1 = (x + i) % height;
			y1 = (y + j) % width;

			if (x1 == -1) {
				x1 = height - 1;
			}
			if (y1 == -1) {
				y1 = width - 1;
			}
			if ((*(field1 + width*x1 + y1) == LIVE) || (*(field1 + width*x1 + y1) == '-'))
				k++;
		}
	}

	if ((*(field1 + width*x + y) == LIVE) || (*(field1 + width*x + y) == '-')) {
		k--;
	}

	return k;
}

void final(int *field1, const int width, const int height) {
	char comanda;
	char file[30];
	FILE* fp;
	for (;;) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int k = check(field1, i, j, width, height);
				if (*(field1 + width*i + j) == LIVE) {
					if ((k < 2) || (k>3)) {
						*(field1 + width*i + j) = '-';
					}
				}
				if (*(field1 + width*i + j) == DEAD) {
					if (k == 3)
					{
						*(field1 + width*i + j) = '+';
					}
				}
			}
		}

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (*(field1 + width*i + j) == '+') {
					*(field1 + width*i + j) = LIVE;
				}
				if (*(field1 + width*i + j) == '-') {
					*(field1 + width*i + j) = DEAD;
				}
				if (*(field1 + width*i + j) == LIVE) { printf("& "); }
				else { printf("- "); }
			}
			printf("\n");
		}
		int i = 0;
		printf("Would you like to save a field? Write 's'\nWould you like to exit? Write 'f'\n ");
		scanf("%c", &comanda);
		if (comanda == 's') {
			printf("Please,write a name of the file\n");
			scanf("%s", file);
			i = strlen(file);
			file[i] = '.';
			file[i + 1] = 't';
			file[i + 2] = 'x';
			file[i + 3] = 't';
			file[i + 4] = '\0';
			fp = fopen(file, "w");
			fprintf(fp,"#Size %u %u\n",height,width);
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					if (*(field1 + width*i + j) == LIVE) {
						fprintf(fp, "%u %u\n", i, j);
					}
				}
			}
			fclose(fp);
		}
		else 
		if (comanda == 'f') {
			break;
		}
	}
}

int main(int argc, char * argv[]) {
	if (1 == argc) {
		printf("%s\n", "Should be more data");
		return -1;
	}

	FILE *fp;
	fp = fopen(argv[1], "r");

	if ((fp = fopen(argv[1], "r")) == NULL) {
		printf("Error with opening file: %s\n", argv[1]);
		fclose(fp);
		return -1;
	}

	unsigned int height = 0;
	unsigned int width = 0;

	int z = fscanf(fp, "#Size %u %u", &height, &width);
	int* field1 = (int*)calloc(height*width, sizeof(int));

	for (;;) {
		int x = 0;
		int y = 0;
		int k = fscanf(fp, "%d%d\n", &x, &y);
		if (k != 2) {
			break;
		}
		*(field1 + x*width + y) = 1;
	}

	final(field1, width, height);
	free(field1);
	fclose(fp);
	return 0;
}