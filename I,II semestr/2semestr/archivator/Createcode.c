#include <stdio.h>
#include "Createcode.h"

HTree *H = NULL;

void Asort(CodeParameters CP, HTree **A) {
	HTree *r;
	for (int i = 0; i < CP.AlphabetSize; i++)
		for (int j = 0; j < CP.AlphabetSize-i-1; j++)
			if (A[j]->v > A[j + 1]->v) {
				r = A[j + 1];
				A[j + 1] = A[j];
				A[j] = r;
			}
}

int CreateCode(char *sourcename, CodeParameters *CP) {
	uchar c;
	uchar s[256];
	HTree **A;

	FILE *F=fopen(sourcename,"rb");
	if (F == NULL) {
		printf("error with openning file.");
		return 0;
	}

	for (int i = 0; i <= 255; i++)
		s[i] = 0;

	
	CP->OrigSize = 0;
	while (!feof(F)) {
		fread(&c, 1, 1, F);
		if (feof(F))
			break;
		s[c]++;
		CP->OrigSize++;
	}

	
	CP->AlphabetSize = 0;
	for (int i = 0; i <= 255; i++)
		if (s[i] != 0)
			CP->AlphabetSize++;
	
	A = (HTree**)malloc(CP->AlphabetSize*sizeof(HTree*));
	for (int i = 0; i < CP->AlphabetSize; i++)
		A[i] = (HTree*)malloc(CP->AlphabetSize*sizeof(HTree));
	
	
	int j = 0;
	for (int i = 0; i < 256; i++)
		if (s[i] != 0) {
			A[j]->c = i;
			A[j]->v = s[i];
			A[j]->left = NULL;
			A[j]->right = NULL;
			j++;
		}
	
	fclose(F);
	
	Asort(*CP, A);
	TreeGenerate(*CP, A);
	CodeGenerate(CP, A);

	return 1;
}

int TreeGenerate(CodeParameters CP, HTree **A) {
	int n_tmp=CP.AlphabetSize;
	HTree *V;

	while (n_tmp > 1) {
		V = (HTree*)malloc(sizeof(HTree));
		if ((n_tmp == 2) || (A[0]->v + A[1]->v) <= (A[1]->v + A[2]->v)) {
			V->right = A[1];
			V->left = A[0];
			V->c = 0;
			V->v = A[0]->v + A[1]->v;
			
			A[0] = V;
			for (int i = 1; i < CP.AlphabetSize-1; i++)
				A[i] = A[i + 1];
		}
		else {
			V->right = A[2];
			V->left = A[1];
			V->c = 0;
			V->v = A[1]->v + A[2]->v;

			A[1] = V;
			for (int i = 2; i < CP.AlphabetSize - 1; i++)
				A[i] = A[i + 1];
		}
		n_tmp--;
	}

	H = A[0];
	printf("N = %d\n", H->v);

}

void CreateIndex(CodeParameters *CP) {
	for (int i = 0; i < 256; i++)
		CP->CT[i] = NULL;
	for (int j = 0; j < CP->AlphabetSize; j++)
		CP->CT[CP->CodeTable[j].c] = &CP->CodeTable[j];
}

int InnerCodeGenerate(HTree *V, Code c1, CodeParameters *CP) {
	Code ctmp = c1;
	static int count = 0;

	if ((V->left == NULL) && (V->right == NULL)) {
		c1.c = V->c;
		CP->CodeTable[count++] = c1;
		return 1;
	}
	
	ctmp.n++;
	ctmp.code[ctmp.n - 1] = 0;
	InnerCodeGenerate(V->left, ctmp, CP);
	ctmp.code[ctmp.n - 1] = 1;
	InnerCodeGenerate(V->right, ctmp, CP);

	return 1;
}

void CodeGenerate(CodeParameters *CP) {
	Code c1;
	c1.n = 0;
	CP->CodeTable = (Code*)malloc(CP->AlphabetSize*sizeof(Code));
	InnerCodeGenerate(H, c1, CP);
	CreateIndex(CP);
}
