#pragma once
#include "Data.h"

typedef struct _HTree {
	uchar c;
	int v;
	struct _HTree *left;
	struct _HTree *right;
} HTree;

int CreateCode(char *, CodeParameters *CP);
void Asort(CodeParameters CP);
int TreeGenerate(CodeParameters CP);
void CodeGenerate(CodeParameters *CP);
void CreateIndex(CodeParameters *CP);