#include <stdio.h>
//#pragma comment (lib,"graf.lib")
#include "../library/graf.h"

#define INF 10000
 
pgraf C;
int **F;
int s, t;
int *D;
int n;

int init(char *filename) {
	C = gread(filename);
	if (C == NULL) {
		printf("error with reading file");
		return 0;
	}
	n = count(C);
	F = (int**)malloc(n*sizeof(int*));
	for (int i = 0; i < n; i++)
		F[i] = (int*)malloc(n*sizeof(int));
	D = (int*)malloc(n*sizeof(int));
	return 1;
}

int deepcount() {
	int u;
	int *used=(int*)malloc(n*sizeof(int));
	for (int i = 0; i < n; i++) {
		D[i] = INF;
		used[i] = 0;
	}

	D[s] = 0;
	u = s;
	while (u>=0) {
		for (int v = 0; v < n; v++)
			if ((used[v] == 0) && ((checkedge(C,u,v) - F[u][v])>0))
				if (D[u] + 1 < D[v])
					D[v] = D[u] + 1;
		used[u] = 1;
		
		u = -1;
		int	mind = INF;
		for (int i = 0; i < n; i++)
			if ((used[i] == 0) && (D[i] < mind)) {
				mind = D[i];
				u = i;
			}
	}
	return (D[t] != INF);
}

int nextway(int u,int minf ) {
	int minf2=minf;
	int flow;
	
	if (u == t)
		return minf;

	for (int v = 0; v < n; v++)
		if ((checkedge(C, u, v) - F[u][v]>0) && ((D[v] - D[u]) == 1)) {
			if ((checkedge(C, u, v) - F[u][v]) < minf2)
				minf2 = checkedge(C, u, v) - F[u][v];
			flow = nextway(v, minf2);
			if (flow > 0) {
				F[u][v] += flow;
				F[v][u] -= flow;
				return flow;
			}
		}
	return 0;
}

int start() {
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			F[i][j] = 0;
	
	while (deepcount())
		while (nextway(s, INF));

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			printf("%d ", F[i][j]);
		printf("\n");
	}
	printf("\n");
	
	int Fs = 0;
	for (int i = 0; i < n; i++)
		Fs += F[s][i];

	return Fs;
}

void freeall() {
	for (int i = 0; i < n; i++) {
		free(F[i]);
	}
	gfree(C);
	free(F);
	free(D);
		
}

int main(int argc,char **argv) {
	if (argc < 2) {
		printf("not enouth parametrs");
		return 0;
	}

	if (!init(argv[1])) {
		printf("error:file isn't exist");
		return 0;
	}
	while (1) {
		scanf("%d", &s);
		if (s == 0)
			return 0;
		scanf("%d", &t);
		if (t == 0)
			return 0;
		
		if ((s > n) || (t > n)||(t<0)||(s<0)) {
			printf("The number of s or r isn't correct.\n");
			continue;
		}
		s--; t--;
		printf("Flow = %d\n", start());

	}

	freeall();
	return 0;
}
