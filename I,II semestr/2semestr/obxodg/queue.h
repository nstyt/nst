#include"stdio.h"
#pragma once

typedef struct _queue {
	int v;
	struct _queue *next;
} queue;

queue* push(queue* L, int v1);
queue* pop(queue*H, int *v1);