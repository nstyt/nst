﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define MAX_PATH 260

#define bool int
#define true 1
#define false 0

#define max(x,y) ((x) > (y) ? (x) : (y))
#define min(x,y) ((x) < (y) ? (x) : (y))
#define DELETE_TMP_FILES 1
static unsigned int MAX_MEMORY_SIZE = 1000000;
static unsigned int LINES_COUNT = 0;
static unsigned int MERGES_TOTAL = 0;
static unsigned int MERGES_DONE = 0;

struct BufferIndex
{
	char* pLine; 
	unsigned int lineLength;
	char* pKeyWord;
	unsigned int keyWordLength; 
};

struct Buffer
{
	char* pData; 
	char* pRest;
	struct BufferIndex* pIndex; 
	size_t nIndexSize; 
};
void swap(struct BufferIndex* a, struct BufferIndex* b);
struct BufferIndex* quickSortPartition(struct BufferIndex* pLeftBound, struct BufferIndex* pRightBound);
bool quickSort(struct BufferIndex* pLeft, struct BufferIndex* pRight);
int split(const char* inFileName, const unsigned int sortColumn);

bool mergeFiles(
	const char* baseFileName,
	const char* outFileName,
	unsigned int nFiles,
	unsigned int nPass,
	unsigned int sortColumn);

int allocateBuffer(struct Buffer* pBuffer);
int freeBuffer(struct Buffer* pBuffer);

int fillBuffer(
	FILE* fileIn,
	const unsigned int sortColumn,
struct Buffer* pBuffer);

int compareStrings(const char* s1, const char* s2, size_t nLength1, size_t nLength2);
void tmpFilesCleanup(const char* fileName);


void copyFile(const char* srcName, const char* dstName)
{
	FILE* fIn = fopen(srcName, "rb");
	FILE* fOut = fopen(dstName, "wb");

	size_t bytesRead = 0;
	char buf[1024];
	do
	{
		bytesRead = fread(buf, 1, 1024, fIn);
		if (bytesRead > 0)
		{
			fwrite(buf, 1, bytesRead, fOut);
		}
	} while (bytesRead == 1024);
	fclose(fIn);
	fclose(fOut);
}

int allocateBuffer(struct Buffer* pBuffer)
{
	if (NULL == pBuffer) return -1;
	pBuffer->pData = (char*)malloc(MAX_MEMORY_SIZE);
	if (NULL == pBuffer->pData)
	{
		printf("not enough memory\n");
		return -1;
	}
	pBuffer->pRest = NULL;
	pBuffer->pIndex = NULL;
	pBuffer->nIndexSize = 0;
	return 0;
}

int freeBuffer(struct Buffer* pBuffer)
{
	if (NULL == pBuffer) return -1;
	if (pBuffer->pData) free(pBuffer->pData);
	if (pBuffer->pIndex) free(pBuffer->pIndex);
	pBuffer->pData = NULL;
	pBuffer->pRest = NULL;
	pBuffer->pIndex = NULL;
	pBuffer->nIndexSize = 0;
	return 0;
}

int fillBuffer(FILE* fileIn, const unsigned int sortColumn, struct Buffer* pBuffer)
{
	if (NULL == pBuffer || pBuffer->pRest >= pBuffer->pData + MAX_MEMORY_SIZE)
	{
		printf("invalid arguments\n");
		return -1;
	}

	size_t filledBytes = 0;
	if (NULL != pBuffer->pRest)
	{
		const size_t restSize = (pBuffer->pData + MAX_MEMORY_SIZE) - pBuffer->pRest;
		for (; filledBytes < restSize; ++filledBytes)
		{
			pBuffer->pData[filledBytes] = pBuffer->pRest[filledBytes];
		}
	}

	const size_t bytesToRead = MAX_MEMORY_SIZE - filledBytes;
	const size_t bytesActuallyRead = fread(pBuffer->pData + filledBytes, 1, bytesToRead, fileIn);
	const bool bEOF = ((bytesActuallyRead < bytesToRead) || feof(fileIn)) ? true : false;
	if (ferror(fileIn))
	{
		printf("error reading input file\n");
		return -1;
	}
	char* pSeek = pBuffer->pData;
	size_t lineCount = 0;
	const char* pEndOfBuffer = pBuffer->pData + min(filledBytes + bytesActuallyRead, MAX_MEMORY_SIZE);
	char* pLastNewLine = NULL;
	for (; pSeek < pEndOfBuffer; ++pSeek)
	{
		if ('\n' == *(pSeek))
		{
			++lineCount;
			pLastNewLine = pSeek;
		}
	}

	if (bEOF && pEndOfBuffer - pLastNewLine > 1)
	{
		++lineCount;
	}

	if (lineCount < 2 && !bEOF)
	{
		printf("file contains lines too big for specified buffer size.\n");
		return -1;
	}

	struct BufferIndex* pIndexArray = NULL;
	if (NULL != pBuffer->pIndex && pBuffer->nIndexSize == lineCount)
	{
		pIndexArray = pBuffer->pIndex;
	}

	if (NULL == pIndexArray)
	{
		if (NULL != pBuffer->pIndex)
		{
			free(pBuffer->pIndex);
			pBuffer->pIndex = NULL;
			pBuffer->nIndexSize = 0;
		}

		pIndexArray = (struct BufferIndex*)malloc(sizeof(struct BufferIndex) * lineCount);
		if (NULL == pIndexArray)
		{
			printf("not enough memory\n");
			return -1;
		}
		memset(pIndexArray, 0, sizeof(struct BufferIndex) * lineCount);
	}
	unsigned int lineNumber = 0, wordNumber = 0;
	char* pWord = pBuffer->pData;
	char *pLine = pBuffer->pData;

	for (pSeek = pBuffer->pData; pSeek < pEndOfBuffer && lineNumber < lineCount; ++pSeek)
	{
		const char c = *pSeek;
		if ('\t' == c || '\n' == c)
		{
			if (sortColumn == wordNumber++)
			{
				(pIndexArray + lineNumber)->pKeyWord = pWord;
				(pIndexArray + lineNumber)->keyWordLength = pSeek - pWord;
			}
			pWord = pSeek + 1;

			if ('\n' == c)
			{
				(pIndexArray + lineNumber)->pLine = pLine;
				(pIndexArray + lineNumber)->lineLength = pSeek - pLine + 1;

				if (wordNumber <= sortColumn)
				{
					free(pIndexArray);
					printf("invalid column index\n");
					return -1;
				}

				++lineNumber;
				pLine = pSeek + 1;
				wordNumber = 0;
			}
		}
	}

	if (true == bEOF && 0 != wordNumber)
	{
		if (sortColumn == wordNumber)
		{
			(pIndexArray + lineNumber)->pKeyWord = pWord;
			(pIndexArray + lineNumber)->keyWordLength = pSeek - pWord;
		}
		(pIndexArray + lineNumber)->pLine = pLine;
		(pIndexArray + lineNumber)->lineLength = pSeek - pLine;
		++lineNumber;
	}
	if (false == bEOF && pLine < pEndOfBuffer)
	{
		pBuffer->pRest = pLine;
	}
	else
	{
		pBuffer->pRest = NULL;
	}
	pBuffer->pIndex = pIndexArray;
	pBuffer->nIndexSize = lineCount;

	return 0;
}

void swap(struct BufferIndex* a, struct BufferIndex* b)
{
	struct BufferIndex tmp = *b;
	*b = *a;
	*a = tmp;
}

int compareStrings(const char* s1, const char* s2, size_t nLength1, size_t nLength2)
{
	int res = strncmp(s1, s2, min(nLength1, nLength2));
	return (0 == res) ? ((int)nLength1 - (int)nLength2) : res;
}

struct BufferIndex* quickSortPartition(struct BufferIndex* pLeftBound, struct BufferIndex* pRightBound)
{
	if (!pLeftBound || !pRightBound)
	{
		printf("invalid input parameters\n");
		return NULL;
	}
	struct BufferIndex* pLeft = pLeftBound;
	struct BufferIndex* pRight = pRightBound;
	struct BufferIndex ref = *pLeftBound;

	while (pLeft < pRight)
	{
		while (compareStrings(pLeft->pKeyWord, ref.pKeyWord, pLeft->keyWordLength, ref.keyWordLength) <= 0 && pLeft < pRightBound) pLeft++;
		while (compareStrings(pRight->pKeyWord, ref.pKeyWord, pRight->keyWordLength, ref.keyWordLength) > 0 && pRight >= pLeftBound) pRight--;
		if (pLeft < pRight)
		{
			swap(pLeft, pRight);
		}
	}

	*pLeftBound = *pRight;
	*pRight = ref;

	return pRight;
}

bool quickSort(struct BufferIndex* pLeft, struct BufferIndex* pRight)
{
	if (!pLeft || !pRight)
	{
		printf("invalid input parameters\n");
		return false;
	}
	bool bLeftRes = false, bRightRes = false;
	struct BufferIndex* pRefItem = NULL;
	if (pLeft < pRight)
	{
		pRefItem = quickSortPartition(pLeft, pRight);
		bLeftRes = quickSort(pLeft, pRefItem - 1);
		bRightRes = quickSort(pRefItem + 1, pRight);
		return bLeftRes && bRightRes;
	}
	return true;
}

int split(const char* inFileName, const unsigned int sortColumn)
{
	FILE* inFile = fopen(inFileName, "rb");
	if (NULL == inFile)
	{
		printf("failed to open input file\n");
		return -1;
	}

	unsigned int fileCount = 0;

	struct Buffer dataBuffer;
	if (allocateBuffer(&dataBuffer) < 0) return -1;

	bool bFail = false;
	while (!ferror(inFile) && !feof(inFile))
	{
		if (fillBuffer(inFile, sortColumn, &dataBuffer) < 0)
		{
			printf("fillBuffer failed\n");
			bFail = true;
			break;
		}

		struct BufferIndex* pBufferIndex = dataBuffer.pIndex;
		const size_t nIndexSize = dataBuffer.nIndexSize;
		bool bSortRes = quickSort(pBufferIndex, pBufferIndex + nIndexSize - 1);
		if (!bSortRes)
		{
			printf("sort failed\n");
			bFail = true;
			break;
		}
		char tmpFileName[MAX_PATH];
		sprintf(tmpFileName, "%s.0.%u", inFileName, fileCount);
		tmpFileName[MAX_PATH - 1] = 0;
		FILE* fileTemp = fopen(tmpFileName, "wb");
		if (NULL == fileTemp)
		{
			printf("failed to create temporary file\n");
			bFail = true;
			break;
		}
		for (size_t i = 0; i < nIndexSize; ++i)
		{
			fwrite((pBufferIndex + i)->pLine, 1, (pBufferIndex + i)->lineLength, fileTemp);
		}

		LINES_COUNT += nIndexSize;
		printf("Splitting file. %u lines processed...\n", LINES_COUNT);

		++fileCount;
		fclose(fileTemp);
	}

	freeBuffer(&dataBuffer);
	fclose(inFile);
	return ferror(inFile) ? -1 : fileCount;
}

void errorCleanup(int errCode, struct Buffer* buf1, struct Buffer* buf2)
{
	if (errCode < 0)
	{
		freeBuffer(buf1);
		freeBuffer(buf2);
	}
}

bool mergeTwoFiles(FILE* f1, FILE* f2, FILE* fOut, unsigned int sortColumn)
{
	struct Buffer buffer1;
	struct Buffer buffer2;
	if (allocateBuffer(&buffer1) < 0) return -1;
	if (allocateBuffer(&buffer2) < 0)
	{
		freeBuffer(&buffer1);
		return -1;
	}

	const int res1 = fillBuffer(f1, sortColumn, &buffer1);
	const int res2 = fillBuffer(f2, sortColumn, &buffer2);
	errorCleanup(res1, &buffer1, &buffer2);
	errorCleanup(res2, &buffer1, &buffer2);
	size_t n1 = 0, n2 = 0;
	while (true)
	{
		while (n1 < buffer1.nIndexSize && n2 < buffer2.nIndexSize)
		{
			if (compareStrings(
				(buffer1.pIndex + n1)->pKeyWord,
				(buffer2.pIndex + n2)->pKeyWord,
				(buffer1.pIndex + n1)->keyWordLength,
				(buffer2.pIndex + n2)->keyWordLength
				) <= 0)
			{
				fwrite((buffer1.pIndex + n1)->pLine, 1, (buffer1.pIndex + n1)->lineLength, fOut);
				++n1;
			}
			else
			{
				fwrite((buffer2.pIndex + n2)->pLine, 1, (buffer2.pIndex + n2)->lineLength, fOut);
				++n2;
			}
		}
		if (n1 == buffer1.nIndexSize)
		{
			if (!feof(f1))
			{
				const int res = fillBuffer(f1, sortColumn, &buffer1);
				errorCleanup(res, &buffer1, &buffer2);
				n1 = 0;
			}
			else break;
		}
		if (n2 == buffer2.nIndexSize)
		{
			if (!feof(f2))
			{
				const int res = fillBuffer(f2, sortColumn, &buffer2);
				errorCleanup(res, &buffer1, &buffer2);
				n2 = 0;
			}
			else break;
		}

	}

	while (true)
	{
		while (n1 < buffer1.nIndexSize)
		{
			fwrite((buffer1.pIndex + n1)->pLine, 1, (buffer1.pIndex + n1)->lineLength, fOut);
			++n1;
		}

		if (!feof(f1))
		{
			const int res = fillBuffer(f1, sortColumn, &buffer1);
			errorCleanup(res, &buffer1, &buffer2);
			n1 = 0;
		}
		else break;
	}

	while (true)
	{
		while (n2 < buffer2.nIndexSize)
		{
			fwrite((buffer2.pIndex + n2)->pLine, 1, (buffer2.pIndex + n2)->lineLength, fOut);
			++n2;
		}

		if (!feof(f2))
		{
			const int res = fillBuffer(f2, sortColumn, &buffer2);
			errorCleanup(res, &buffer1, &buffer2);
			n2 = 0;
		}
		else break;
	}

	freeBuffer(&buffer1);
	freeBuffer(&buffer2);
	return true;
}

bool mergeFiles(const char* baseFileName, const char* outFileName, unsigned int nFiles, unsigned int nPass, unsigned int sortColumn)
{
	if (0 == nFiles)
	{
		return true;
	}
	else if (1 == nFiles)
	{
		char path[MAX_PATH];
		sprintf(path, "%s.%u.%u", baseFileName, nPass, 0);
		path[MAX_PATH - 1] = 0;
		FILE* f = fopen(path, "rb");
		if (NULL == f)
		{
			printf("can't open temporary file \n");
			return false;
		}
		fclose(f);
#if DELETE_TMP_FILES
		rename(path, outFileName);
#else
		copyFile(path, outFileName);
#endif
		return true;
	}
	else
	{
		int odd = nFiles % 2;
		char path1[MAX_PATH];
		char path2[MAX_PATH];
		char pathOut[MAX_PATH];
		for (unsigned int i = 0; i < nFiles / 2; ++i)
		{
			sprintf(path1, "%s.%u.%u", baseFileName, nPass, 2 * i);
			sprintf(path2, "%s.%u.%u", baseFileName, nPass, 2 * i + 1);
			sprintf(pathOut, "%s.%u.%u", baseFileName, nPass + 1, i);
			path1[MAX_PATH - 1] = 0;
			path2[MAX_PATH - 1] = 0;
			pathOut[MAX_PATH - 1] = 0;

			FILE* f1 = fopen(path1, "rb");
			if (NULL == f1)
			{
				printf("mergeFiles: can't open temporary file %s\n", path1);
				return false;
			}
			FILE* f2 = fopen(path2, "rb");
			if (NULL == f2)
			{
				printf("mergeFiles: can't open temporary file %s\n", path2);
				fclose(f1);
				return false;
			}
			FILE* fOut = fopen(pathOut, "wb");
			if (NULL == fOut)
			{
				printf("mergeFiles: can't open temporary file %s\n", pathOut);
				fclose(f1);
				fclose(f2);
				return false;
			}
			bool bRes = mergeTwoFiles(f1, f2, fOut, sortColumn);
			fclose(f1);
			fclose(f2);
			fclose(fOut);

			MERGES_DONE += 1;
			printf("Merging files: (%u/%u)\n", MERGES_DONE, MERGES_TOTAL);

#if DELETE_TMP_FILES
			remove(path1);
			remove(path2);
#endif
		}
		if (odd > 0)
		{
			char path1[MAX_PATH];
			char path2[MAX_PATH];
			sprintf(path1, "%s.%u.%u", baseFileName, nPass, nFiles - 1);
			sprintf(path2, "%s.%u.%u", baseFileName, nPass + 1, nFiles / 2);
			path1[MAX_PATH - 1] = 0;
			path2[MAX_PATH - 1] = 0;

#if DELETE_TMP_FILES
			rename(path1, path2);
#else
			copyFile(path1, path2);
#endif
		}
		return true;
	}
}

int main(int argc, char* argv[])
{
	const char* inFileName = NULL;
	const char* outFileName = NULL;
	unsigned int columnIndex = 0;
	bool parseParametersSuccess = true;

	for (int i = 0; i < argc; ++i)
	{
		if (0 == strcmp(argv[i], "-in"))
		{
			if (i + 1 < argc)
			{
				inFileName = argv[i + 1];
			}
			else
			{
				parseParametersSuccess = false;
				break;
			}
		}
		else if (0 == strcmp(argv[i], "-out"))
		{
			if (i + 1 < argc)
			{
				outFileName = argv[i + 1];
			}
			else
			{
				parseParametersSuccess = false;
				break;
			}
		}
		else if (0 == strcmp(argv[i], "-col"))
		{
			if (i + 1 < argc)
			{
				columnIndex = strtol(argv[i + 1], 0, 10);
			}
			else
			{
				parseParametersSuccess = false;
				break;
			}
		}
		else if (0 == strcmp(argv[i], "-mem"))
		{
			if (i + 1 < argc)
			{
				MAX_MEMORY_SIZE = strtol(argv[i + 1], 0, 10);
			}
			else
			{
				parseParametersSuccess = false;
				break;
			}
		}
	}

	if (!parseParametersSuccess || !inFileName || !outFileName)
	{
		printf("Invalid input parameters. Usage:\n\t -in input_file_name\n\t -out output_file_name\n\t -col column_index\n\t -mem buffer_size\n\n");
		return EXIT_FAILURE;
	}

	const int numFiles = split(inFileName, columnIndex);
	if (numFiles < 0)
	{
		printf("\nFile sort failed.\n");
		tmpFilesCleanup(inFileName);
		return EXIT_FAILURE;
	}

	int n = numFiles;
	while (n > 1)
	{
		MERGES_TOTAL += n / 2;
		n = n / 2 + n % 2;
	}

	remove(outFileName);
	int nFilesLeft = numFiles;
	int nPass = 0;
	bool bRes = true;
	while (true == bRes)
	{
		bRes = mergeFiles(inFileName, outFileName, nFilesLeft, nPass, columnIndex);
		if (nFilesLeft == 1) break;
		nFilesLeft = nFilesLeft / 2 + nFilesLeft % 2;
		++nPass;
	}

	if (false == bRes)
	{
		printf("\nFile sort failed.\n");
		tmpFilesCleanup(inFileName);
		return EXIT_FAILURE;
	}

#if !DELETE_TMP_FILES
	tmpFilesCleanup(inFileName);
#endif
	printf("\nFile successfully sorted!\n");
	return EXIT_SUCCESS;
}

void tmpFilesCleanup(const char* fileName)
{
	unsigned int nPass = 0;
	unsigned int nFile = 0;
	char buf[MAX_PATH];
	while (true)
	{
		nFile = 0;
		while (true)
		{
			sprintf(buf, "%s.%u.%u\0", fileName, nPass, nFile);
			FILE* f = fopen(buf, "r");
			if (NULL == f)
			{
				break;
			}
			else
			{
				fclose(f);
				remove(buf);
				++nFile;
			}
		}

		if (nFile == 0)
		{
			break;
		}
		else
		{++nPass;}
	}
}
