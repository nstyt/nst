#include<stdio.h>;

FILE *f;
int M, N;
char **L;
int Ax, Ay, Bx, By;
int x, y;

void printl();

int read(char*filename) {
	f = fopen(filename, "r");
	char c = 0;
	int slen;
	if (f == NULL) {
		printf("Error: file is not opened.\n");
		return 0;
	}
	while ((c != '\n') && (c != '\r'))
		fread(&c, 1, 1, f);

	M = ftell(f)-2;
	fseek(f,0,SEEK_END);
	N = (ftell(f) - 1) / M;
	fseek(f, 0, SEEK_SET);
	printf("%d %d\n", M, N);
	L = (char**)malloc(N*sizeof(char*));
	for (int i = 0; i < N; i++)
		L[i] = (char*)malloc(M*sizeof(char));

	slen = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			fscanf(f, "%c", &c);
			if (c == 'A') {
				Ax = i;
				Ay = j;
				c = ' ';
			}
			if (c == 'B') {
				Bx = i;
				By = j;
				c = ' ';
			}
			if (c != '\n') {
				L[i][j] = c;
				slen++;
			}
			else {
				j--;
				if (slen != M) {
					printf("Maze isn't correct.\n");
					return 0;
				}
				slen = 0;
			}
		}
	}
	return 1;	
}

int findchance(int *x,int *y, int dx, int dy, char p) {
	if ((*x+dx >= 0) && (*y + dy >= 0) && (*x + dx < N) && (*y + dy < M)) 
		if (((p == 0) && (L[*x + dx][*y + dy] == ' ')) || ((p == 1) && (L[*x+dx][*y+dy] == '*')))
		{
			*x += dx;
			*y += dy;
			return 1;
		}
	return 0;
}

int W(int B, int x) {
	if (B > x)
		return 1;
	else
		return -1;
}

int nextStep() {
	if ((x == Bx) && (y == By))
		return 2;

	if ((findchance(&x, &y, W(Bx, x), 0, 0)) || 
		(findchance(&x, &y, 0, W(By, y), 0)) ||
		(findchance(&x, &y, -W(Bx, x), 0, 0)) ||
		(findchance(&x, &y, 0, -W(By, y), 0)))
	{
		L[x][y] = '*';
		return 1;
	}

	L[x][y] = 'x';
	if ((findchance(&x, &y, -W(Bx, x), 0, 1)) ||
		(findchance(&x, &y, 0, -W(By, y), 1)) ||
		(findchance(&x, &y, W(Bx, x), 0, 1)) ||
		(findchance(&x, &y, 0, W(By, y), 1)))
		return 1;

	return -1;
}

int findway() {
	int res = 1;
	
	x = Ax;
	y = Ay;
	L[x][y] = '*';
	while (res == 1) 
		res = nextStep();

	if (res == 2)
		printl();
	else
		printf("Way is not exist.\n");
	return 0;
}

void printl() {
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++)
			if ((i == Ax) && (j == Ay))
					printf("A ");
			else if ((i == Bx) && (j == By))
				printf("B ");
			else if ((L[i][j] == ' ') || (L[i][j] == 'x'))
				printf("_ ");
			else
				printf("%c ", L[i][j]);
		printf("\n");

	}
	printf("\n");

}

int main(int argc,char**argv) {
	if (argc < 2) {
		printf("error:not enoth arguments");
		return 0;
	}

	if (!read(argv[1])) {
		return 0;
	}
	
	printl();
	findway();

	return 0;
}