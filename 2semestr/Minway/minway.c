#include "../library/graf.h"
//#pragma comment (lib,"graf.lib")
#include "stdio.h"
#define INF 32767 
#define min(a,b) ((a)<(b))?(a):(b)

int nextv(int v,int *visited, int *D) {
	int verg=-1;
	int value = INF;
	for (int i = 0; i < v; i++) {
		if ((visited[i] == 0) && (D[i]<value)) {
			verg = i;
			value = D[i];
		}
	}
	return verg;
}



void way(pgraf A,int v,int s,int *D) {
	int *visited= (int*)malloc(v*sizeof(int));
	int u;

	for (int i = 0; i < v; i++) {
		D[i] = INF;
		visited[i] = 0;
	}
	D[s] = 0;
 
	u = s;
	while (u!=-1) {
		for (int i = 0; i < v; i++)
			if (checkedge(A,u,i)>0)
				D[i] = min(D[i],D[u]+ checkedge(A, u, i));

		visited[u] = 1;
		u = nextv(v, visited, D);
	}
	free(visited);
	return D;
}


void main(int argc,char **argv) {

	if (argc == 1) {
		printf("error input parametrs");
		return;
	}

	pgraf A;
	int *D;
	int s, t;
	A = gread(argv[1]);
	if (A == NULL) {
		printf("file is not exist");
		return;
	}
		
	D = (int*)malloc(count(A)*sizeof(int));
	
	while (1) {
		scanf("%d%d", &s, &t);
		if ((s == 0)||(t==0)|| (s < 1) || (t < 1))
			break;
		
		way(A,count(A), s-1, D);
		if (D[t] < INF)
			printf("Distance between %d, %d = %d\n", s, t, D[t-1]);
		else
			printf("there are no way to that verge\n");
	}
}