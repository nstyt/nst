#pragma once

#define SMEGNOST 0
#define INCENDENTNOST 1
#define EDGELIST 2

typedef void* pgraf;

pgraf gcreate(int v,int e,int type_o, int type_v, int placesave);
pgraf gread(char* filename);
int checkedge(pgraf pg, int i, int j);
void gfree(pgraf* pg);
int addedge(pgraf pg, int x, int y, int w);
int deletedge(pgraf pg, int x, int y);
pgraf gcopy(pgraf pg);
int getweight(pgraf pg, int x, int y);
int gsave(pgraf pg, char* filename);

int count(pgraf pg);

