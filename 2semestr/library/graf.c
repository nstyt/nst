#include "graf.h"
#include <stdio.h>
#include <stdlib.h>


typedef struct graf {
	int type_o;
	int type_v;
	int placesave;
	int v;
	int e;
	int **A;
} graf;

pgraf gcreate(int v, int e, int type_o, int type_v, int placesave) {
	graf*g;
	if (e > v*v) {
		printf("Error (gcreate): the number of edge is too large.\n");
		return NULL;
	}

	g = (graf*)malloc(sizeof(graf));
	g->v = v;
	g->e = e;
	g->type_o = type_o;
	g->type_v = type_v;
	g->placesave = placesave;
	
	if (g->placesave == SMEGNOST) {
		g->A = (int**)malloc(g->v*sizeof(int*));
		for (int i = 0; i < g->v; i++)
			g->A[i] = (int*)malloc(g->v*sizeof(int));
		return g;
	}
	
	if (g->placesave == EDGELIST) {
		if (g->type_v == 0) {
			g->A = (int**)malloc(2 * sizeof(int*));
			g->e = 0;
			for (int i = 0; i < 2; i++)
				g->A[i] = NULL;//(int*)calloc(g->e, sizeof(int));
			
			return g;
		}
		
		g->A = (int**)malloc(3 * sizeof(int*));
		g->e = 0;
		for (int i = 0; i < 3; i++)
			g->A[i] = NULL;// (int*)calloc(g->e, sizeof(int));
		
		return g;
	}

	if (g->placesave == INCENDENTNOST) {
		g->A = (int**)malloc(g->v*sizeof(int*));
		for (int i = 0; i < g->v; i++)
			g->A[i] = (int*)calloc(g->e, sizeof(int));
		
		return g;
	}

	return NULL;
}

pgraf gread(char* filename) {
	graf*g;
	int v,
		e = 0,
		placesave,
		type_o,
		type_v;
	
	FILE *F = fopen(filename, "r");
	if (F == NULL) {
		printf("Error (gread): file cannot be opened.\n");
		return NULL;
	}
	
	fscanf(F, "%d %d %d", &type_o, &type_v, &placesave);
	if (placesave == SMEGNOST) {
		fscanf(F, "%d", &v);
		g = (graf*)gcreate(v,e,type_o,type_v,placesave);
		for (int i = 0; i < g->v; i++)
			for (int j = 0; j < g->v; j++) {
				fscanf(F, "%d ", &g->A[i][j]);
			}
	}

	if (placesave == INCENDENTNOST) {
		fscanf(F, "%d %d", &v,&e);
		g = (graf*)gcreate(v, e, type_o, type_v, placesave);
		for (int i = 0; i < g->v; i++)
			for (int j = 0; j < g->e; j++) {
				fscanf(F, "%d ", &g->A[i][j]);
			}
	}

	if (placesave == EDGELIST) {
		fscanf(F, "%d %d", &v,&e);
		g = (graf*)gcreate(v, e, type_o, type_v, placesave);
		if (type_v == 0)
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < g->e; j++) {
					fscanf(F, "%d ", &g->A[i][j]);
				}
		else
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < g->e; j++) {
					fscanf(F, "%d ", &g->A[i][j]);
				}
	}

	return g;
}

void gfree(pgraf *pg) {
	if (pg == NULL) {
		printf("Error(gfree): graf is not exist.\n");
		return NULL;
	}

	graf*g=(graf*)*pg;

	if (g->placesave == SMEGNOST) {
		for (int i = 0; i < g->v; i++)
			free(g->A[i]);
	}

	if ((g->placesave == INCENDENTNOST) || (g->placesave == EDGELIST)) {

		for (int i = 0; i < g->e; i++)
			free(g->A[i]);
	}
	free(g->A);
	free(g);
	
	*pg = NULL;
}

int checkedge(pgraf pg, int i, int j) {
	if (pg == NULL) {
		printf("Error(checkedge):graf is not exist.\n");
		return -1;
	}
	graf*g = (graf*)pg;

	if (g->placesave == SMEGNOST) {
		if (g->type_o) {
			if (g->A[i][j]>0)
				return g->A[i][j];
			else
				return 0;
		}
		else
		{
			if (g->A[i][j]>0)
				return g->A[i][j];
			if (g->A[j][i]>0)
				return g->A[j][i];
			return 0;
		}
	}

	if (g->placesave == INCENDENTNOST) {
		if (g->type_o) {
			for (int k = 0; k < g->e; k++)
				if ((g->A[j][k] < 0) && (g->A[i][k] > 0))
					return g->A[i][k];
			return 0;
		}
		else
		{
			for (int k = 0; k < g->e; k++)
				if ((g->A[j][k]>0) && (g->A[i][k] > 0))
					return g->A[j][k];
			return 0;
		}
	}

	if (g->placesave == EDGELIST) {
		if (g->type_o == 1) {
			for (int k = 0; k < g->e; k++)
				if ((g->A[0][k] == i) && (g->A[1][k] == j))
					if (g->type_v == 1)
						return g->A[2][k];
					else
						return 1;
			return 0;
		}
		else
		{
			for (int k = 0; k < g->e; k++)
				if (((g->A[0][k] == i) && (g->A[1][k] == j)) ||
					((g->A[1][k] == i) && (g->A[0][k] == j)))
					if (g->type_v == 1)
						return g->A[2][k];
					else
						return 1;
			return 0;
		}
	}

	return -1;
}

int addedge(pgraf pg, int x, int y, int w) {
	if (pg == NULL) {
		printf("error(addedge):graf is not exist.\n");
		return -1;
	}

	graf*g = (graf*)pg;
	if ((x >= g->v) || (y >= g->v) || (w < 0)) {
		printf("error(addedge):arguments are not correct.\n");
		return -1;
	}

	if (!g->type_v)
		w = 1;

	if (g->placesave == SMEGNOST) {
		if (!checkedge(pg, x, y)) {
			g->A[x][y] = w;
			if (!g->type_o)
				g->A[y][x] = w;
		}
		else
			return 0;
	}

	if (g->placesave == INCENDENTNOST) {
		if (!checkedge(pg, x, y)) {
			for (int i = 0; i < g->v; i++)
				g->A[i] = (int*)realloc(g->A[i], (g->e + 1)*sizeof(int));
			g->e++;
			for (int i = 0; i < g->v; i++)
				g->A[i][g->e - 1] = 0;

			if (g->type_o)
				g->A[x][g->e - 1] = -w;
			else
				g->A[x][g->e - 1] = w;
			g->A[y][g->e - 1] = w;

		}
		else
			return 0;
	}

	if (g->placesave == EDGELIST) {
		if (!checkedge(pg, x, y)) {
			g->e++;
			for (int i = 0; i < 2; i++)
				g->A[i] = (int*)realloc(g->A[i], g->e*sizeof(int));
			g->A[0][g->e - 1] = x;
			g->A[1][g->e - 1] = y;

			if (g->type_v) {
				g->A[2] = (int*)realloc(g->A[2], g->e*sizeof(int));
				g->A[2][g->e - 1] = w;
			}
		}
		else
			return 0;
	}

	return 1;
}

int deletedge(pgraf pg,int x,int y) {
	if (pg == NULL) {
		printf("error(deletedge):graf is not exist.\n");
		return -1;
	}
	graf*g = (graf*)pg;
	if ((x >= g->v) || (y >= g->v)) {
		printf("error(deletedge):arguments are not correct.\n");
		return -1;
	}

	if (!checkedge(pg, x, y))
		return 0;

	g->e--;
	if ( g->placesave == SMEGNOST) {
		g->A[x][y] = 0;
		if (!g->type_o)
			g->A[y][x] = 0;
		return 1;
	}

	if (g->placesave == INCENDENTNOST) {
		if (g->type_o) {
			for (int i = 0; i <= g->e; i++)
				if ((g->A[x][i] < 0) && (g->A[y][i]>0)) {
					for (int j = i; j < g->e; j++)
						for (int k = 0; k < g->v; k++)
							g->A[k][j] = g->A[k][j + 1];

					for (int j = 0; j < g->v; j++)
						g->A[j] = (int*)realloc(g->A[j], g->e*sizeof(int));
					return 1;
				}
		}
		else
		{
			for (int i = 0; i <= g->e;i++)
				if ((g->A[x][i] > 0) && (g->A[y][i]>0)) {
					for (int j = i; j < g->e; j++)
						for (int k = 0; k < g->v; k++)
							g->A[k][j] = g->A[k][j + 1];

					for (int j = 0; j < g->v; j++)
						g->A[j] = (int*)realloc(g->A[j], g->e*sizeof(int));
					return 1;
				}
		}
	}

	if (g->placesave == EDGELIST)
		for (int i = 0; i <= g->e; i++)
			if (((g->A[0][i] == x) && (g->A[1][i] == y)) ||
				((g->A[0][i] == y) && (g->A[1][i] == x) && (!g->type_o)))
				if (g->type_v) {
					for (int j = i; j < g->e; j++)
						for (int k = 0; k < 3; k++)
							g->A[k][j] = g->A[k][j + 1];
					g->A[0] = (int*)realloc(g->e, sizeof(int));
					g->A[1] = (int*)realloc(g->e, sizeof(int));
					g->A[2] = (int*)realloc(g->e, sizeof(int));
					return 1;
				}
				else {
					for (int j = i; j < g->e; j++)
						for (int k = 0; k < 2; k++)
							g->A[k][j] = g->A[k][j + 1];
					g->A[0] = (int*)realloc(g->e, sizeof(int));
					g->A[1] = (int*)realloc(g->e, sizeof(int));
					return 1;
				}

	return 0;
}

pgraf gcopy(pgraf pg) {
	if (pg == NULL) {
		printf("error(gcopy):graf is not exist.\n");
		return NULL;
	}

	graf*g = (graf*)pg;
	graf*g2 = (graf*)malloc(sizeof(graf));

	g2->placesave = g->placesave;
	g2->v = g->v;
	g2->e = g->e;
	g2->type_o = g->type_o;
	g2->type_v = g->type_v;

	if (g2->placesave == SMEGNOST) {
		g2->A = (int**)malloc(g2->v*sizeof(int*));
		for (int i = 0; i < g2->v; i++)
			g2->A[i] = (int*)malloc(g2->v*sizeof(int));
		for (int i = 0; i < g2->v; i++)
			for (int j = 0; j < g->v; j++)
				g2->A[i][j] = g->A[i][j];
		return g2;
	}
	if (g2->placesave == INCENDENTNOST) {
		g2->A = (int**)malloc(g2->v*sizeof(int*));
		for (int i = 0; i < g2->v; i++)
			g2->A[i] = (int*)malloc(g2->e*sizeof(int));
		for (int i = 0; i < g2->v; i++)
			for (int j = 0; j < g->e; j++)
				g2->A[i][j] = g->A[i][j];
		return g2;
	}
	if (g2->placesave == EDGELIST) {
		if (g2->type_v) {
			g2->A = (int**)malloc(3*sizeof(int*));
			for (int i = 0; i < 3; i++)
				g2->A[i] = (int*)malloc(g2->e*sizeof(int));
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < g->e; j++)
					g2->A[i][j] = g->A[i][j];
			return g2;
		}
		else {
			g2->A = (int**)malloc(2 * sizeof(int*));
			for (int i = 0; i < 2; i++)
				g2->A[i] = (int*)malloc(g2->e*sizeof(int));
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < g->e; j++)
					g2->A[i][j] = g->A[i][j];
			return g2;
		}
	}

	return NULL;
}

int getweight(pgraf pg, int x, int y) {
	if (pg == NULL) {
		printf("error(getweight):graf is not exist.\n");
		return -1;
	}
	return checkedge(pg, x, y);
}

int gsave(pgraf pg, char* filename) {
	if (pg == NULL) {
		printf("error(gsave):graf is not exist.\n");
		return -1;
	}
	FILE*f = fopen(filename,"w");
	if (f == NULL) {
		printf("error(gsave):File can't be opened.\n");
		return 0;
	}
	graf* g = (graf*)pg;
	fprintf(f, "%d %d %d %d", g->type_o, g->type_v, g->placesave, g->v);
	if (g->placesave == SMEGNOST) {
		fprintf(f, "\n");
		for (int i = 0; i < g->v; i++){
			for (int j = 0; j < g->v; j++) 
				fprintf(f, "%d ", g->A[i][j]);
			fprintf(f, "\n");
		}
		return 1;
	}
	if (g->placesave == INCENDENTNOST) {
		fprintf(f, "%d\n",g->e);
		for (int i = 0; i < g->v; i++) {
			for (int j = 0; j < g->e; j++)
				fprintf(f, "%d ", g->A[i][j]);
			fprintf(f, "\n");
		}
		return 1;
	}
	if (g->placesave == EDGELIST) {
		fprintf(f, "%d\n", g->e);
		if (g->type_v) {
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j <g->e; j++)
					fprintf(f, "%d ", g->A[i][j]);
				fprintf(f, "\n");
			}
		}
		else {
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j <g->e; j++)
					fprintf(f, "%d ", g->A[i][j]);
				fprintf(f, "\n");
			}
		}
		return 1;
	}

	return 0;
}

int count(pgraf pg) {
	if (pg == NULL) {
		printf("error(count):graf is not exist.\n");
		return -1;
	}
	return ((graf*)pg)->v;
}