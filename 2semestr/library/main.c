#include "graf.h"
#include <stdio.h>

int main() {
	pgraf g = gread("gjgg");
	g = gread("graf.txt");
	
	for (int i = 0; i < count(g); i++) {
		for (int j = 0; j < count(g); j++) 
			printf("%d ", checkedge(g,i,j));
		printf("\n");
	}

	pgraf g2 = gcopy(g);
	addedge(g2, 0, 2, 1);
	addedge(g2, 0, 1, 1);
	deletedge(g2, 1, 2);

	//������������ ������� ���������
	addedge(g2, 0, 4, 0);
	deletedge(g2, 0, 3);

	printf("\n");
	for (int i = 0; i < count(g2); i++) { 
		for (int j = 0; j < count(g2); j++)
			printf("%d ", getweight(g2, i, j));
		printf("\n");
	}

	gsave(g2,"graf2.txt");

	gfree(&g);
	gfree(&g2);
	checkedge(g, 0, 1);

	gfree(NULL);

	pgraf g3 = gcreate(2,5,0,1,INCENDENTNOST);
	addedge(g3, 0, 1, 0);

	g3 = gcreate(4, 0, 1, 0, EDGELIST);
	addedge(g3, 2, 3, 0);
	addedge(g3, 1, 1, 0);

	printf("\n");
	for (int i = 0; i < count(g3); i++) {
		for (int j = 0; j < count(g3); j++)
			printf("%d ", getweight(g3, i, j));
		printf("\n");
	}

	return 0;
}