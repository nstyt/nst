#include "ArchProcessing.h"

int writeHeader(FILE *fout, char*sourcename, CodeParameters CP) {
	uchar k=strlen(sourcename);
	fwrite("ID:234;", 1, 7, fout);
	fwrite(&k,1,1,fout);
	fwrite(sourcename, 1, k, fout);
	fwrite(&CP.OrigSize, sizeof(int), 1, fout);
	fwrite(&CP.AlphabetSize, sizeof(int), 1, fout);

	for (int i = 0; i < CP.AlphabetSize; i++) {
		fwrite(&CP.CodeTable[i].c, sizeof(char), 1, fout);
		fwrite(&CP.CodeTable[i].n, sizeof(int), 1, fout);
		fwrite(CP.CodeTable[i].code, sizeof(char), CP.CodeTable[i].n, fout);
	}
	printf("HeaderSize: %d\n", ftell(fout));
	return 1;
}

void WriteBits(uchar *outc, uchar c, int code_start, int count, int outc_start, CodeParameters CP) {
	for (int i = code_start; i < code_start + count; i++) 
		*outc |= CP.CT[c]->code[i] << (7 - outc_start - (i - code_start));
}

void ViewBits(uchar outc) {
	for (int i = 0; i < 8; i++)
		if (outc&(1 << (7 - i)))
			printf("1");
		else
			printf("0");
}

int Archivate(char *sourcename, char *archname, CodeParameters CP) {
	uchar outc,
		  inc;
	int outfilled = 0;
	int outwritten = 0;
	FILE *fout;
	FILE *fin;
	fout = fopen(archname, "wb");
	if (!fout) {
		printf("Error:File <archname> isn't opened.\n");//////
		return 0;
	}

	fin = fopen(sourcename, "rb");
	if (!fin) {
		printf("Error:File <sourcename> isn't opened.\n");///////
		return 0;
	}

	writeHeader(fout,sourcename,CP);
	
	outc = 0;
	outfilled = 0;
	while (!feof(fin)) {
		fread(&inc, 1, 1, fin);
		
		if (8 - outfilled >= CP.CT[inc]->n) {
			WriteBits(&outc, inc, 0, CP.CT[inc]->n, outfilled, CP);
			outfilled += CP.CT[inc]->n;
			
			if (outfilled == 8) {
				fwrite(&outc, 1, 1, fout);
				outc = 0;
				outfilled = 0;
			}
		}
		else {
			outwritten = 0;
			WriteBits(&outc, inc, outwritten, 8 - outfilled, outfilled, CP);
			outwritten += 8 - outfilled;
			
			fwrite(&outc, 1, 1, fout);
			outc = 0;
			outfilled = 0;

			while (CP.CT[inc]->n - outwritten >= 8) {
				WriteBits(&outc, inc, outwritten, 8, 0, CP);
				outwritten += 8;
				
				fwrite(&outc, 1, 1, fout);
				outc = 0;
			}

			if (CP.CT[inc]->n - outwritten > 0) {
				WriteBits(&outc, inc, outwritten, CP.CT[inc]->n - outwritten, 0, CP);
				outfilled += CP.CT[inc]->n - outwritten;
				outwritten += CP.CT[inc]->n - outwritten;
			}
		}
	}

	if (outfilled>0)
		fwrite(&outc, 1, 1, fout);

	printf("Archivation has completed.\n");

	fclose(fout);
	fclose(fin);

	return 1;
}