#pragma once

typedef unsigned char uchar;

typedef struct _Code {
	uchar c;
	int n;
	char code[256];
} Code;

typedef struct _CodeParameters {
	int AlphabetSize,
		OrigSize;
	
	Code *CodeTable;
	Code *CT[256];
} CodeParameters;

