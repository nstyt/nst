#include "ExtractProcessing.h"
#include <stdio.h>

FILE* readHeader(FILE *fin, CodeParameters *CP, int isourceNameOrig) {
	char ID[7];
	uchar k;
	char *destname;

	fread(ID, 1, 7, fin);
	ID[6] = 0;

	if (strcmp(ID, "ID:234") != 0) {
		printf("Error:Wrong archive format.\n");
		return 0;
	}

	fread(&k, sizeof(uchar), 1, fin);
	if (isourceNameOrig) {
		destname = (char*)malloc((k + 1)*sizeof(char));
		fread(destname, sizeof(char), k, fin);
		destname[k] = 0;
	}
	else 
		fseek(fin, k, SEEK_CUR);

	fread(&CP->OrigSize, sizeof(int), 1, fin);
	fread(&CP->AlphabetSize, sizeof(int), 1, fin);
	printf("Orig size: %d\nAlphabet size: %d\n", CP->OrigSize, CP->AlphabetSize);

	CP->CodeTable = (Code*)malloc(CP->AlphabetSize*sizeof(Code));
	for (int i = 0; i < CP->AlphabetSize; i++) {
		fread(&CP->CodeTable[i].c, 1, 1, fin);
		fread(&CP->CodeTable[i].n, sizeof(int), 1, fin);
		fread(CP->CodeTable[i].code, 1, CP->CodeTable[i].n, fin);
	}

	return fopen(destname, "wb");
}

int SearchCT(Code *cout, CodeParameters CP) {
	int k;
	for (int i = 0; i < CP.AlphabetSize; i++)
		if (CP.CodeTable[i].n == cout->n) {
			k = 1;
			for (int j = 0; j < cout->n; j++)
				if (cout->code[j] != CP.CodeTable[i].code[j]) {
					k = 0;
					break;
				}
			if (k == 1) {
				cout->c = CP.CodeTable[i].c;
				return 1;
			}
		}
	return 0;
}

int GetBit(uchar c, int number) {
	return ((c >> (7 - number))&1);
}

int extractArch(char *archname, char *destname, CodeParameters *CP, int isourceNameOrig) {
	int curBit=0,
		charcount=0,
		flag;

	Code cout;
	uchar inc;
	
	FILE *fin;
	FILE *fout;
	
	fin=fopen(archname,"rb");
	if (!fin) {
		printf("Error with openning infile.\n");
		return 0;
	}
	
	fout = readHeader(fin, CP, isourceNameOrig);
	
	if (!fout) {
		printf("Error with openning outfile.\n");
		return 0;
	}

	cout.c = 0;
	cout.n = 0;
	fread(&inc, 1, 1, fin);
	while (!feof(fin)) {
		while (cout.n <= CP->AlphabetSize) {
			if (curBit == 8) {
				if (!feof(fin))
					fread(&inc, 1, 1, fin);
				else
					break;
				curBit = 0;
			}
			cout.code[cout.n] = GetBit(inc, curBit);
			cout.n++;
			curBit++;

			flag = SearchCT(&cout, *CP);
			if (flag)
				break;
		}
	
		if (flag && (cout.n <= CP->AlphabetSize) && (charcount < CP->OrigSize)) { 
			charcount++;
			fwrite(&cout.c, 1, 1, fout);
			cout.c = 0;
			cout.n = 0;
			continue;
		}
		else
			if (feof(fin) || (charcount == CP->OrigSize)) {
				printf("Extraction has finished.\n");
				break;
			}
			else {
				printf("Archive isn't correct.\n");
				break;
			}
	}

	fclose(fin);
	fclose(fout);
	
	return 1;
}
