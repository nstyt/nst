#include <stdio.h>
#include "Createcode.h"
#include "ArchProcessing.h"
#include "ExtractProcessing.h"
#include "Data.h"

int main(int argc, char **argv) {
	char *sourcename;
	char *archname;
	CodeParameters CP;

	if (argc < 3) {
		printf("error with a number of arguments.");
		return 0;
	}

	if (strcmp(argv[1], "-a") == 0) {
		sourcename = argv[2];

		if ((argc >= 5) && (strcmp(argv[3], "-o") == 0)){
			archname = argv[4];
		}
		else {
			archname = (char*)malloc(strlen(sourcename + 4)*sizeof(char));
			strcpy(archname, sourcename);
			strcat(archname, ".arc");
		}
		
		if (!CreateCode(sourcename,&CP))
			return 0;
		Archivate(sourcename,archname, CP);

		return 1;
	}
	
	if (strcmp(argv[1], "-x") == 0) {
		archname = argv[2];
		if ((argc >= 5) && (strcmp(argv[3], "-o") == 0)) {
			sourcename = argv[4];
			extractArch(archname, sourcename, &CP, 0);
		}
		else
			extractArch(archname,NULL, &CP, 1);
	}
	
	return 1;
}