
#include "stdio.h"
#include "../library/graf.h"
//#pragma comment (lib,"graf.lib")
#include "queue.h"

pgraf A;
int *used;

int godeep(int v) {
	used[v] = 1;
	printf("%d ", v);
	for (int i = 0; i < count(A); i++)
		if (checkedge(A, v, i) && (used[i] == 0))
			godeep(i);

	return 0;
}

void gowidth() {
	queue *H, *L = NULL;
	int v;

	for (int i = 0; i < count(A); i++)
		used[i] = 0;

	H = push(L, 0);
	L = H;
	used[0] = 1;
	while (H != NULL) {
		H = pop(H,&v);
		printf("%d ", v);
		for (int i = 0; i < count(A); i++) {
			if (checkedge(A, v, i) && (used[i] == 0)) {
				L = push(L, i);
				if (H == NULL)
					H = L;
				used[i] = 1;
			}
		}		
	}
	printf("\n");
}

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("error input parametrs");
		return;
	}
	A = gread(argv[1]);
	if (A==NULL) {
		printf("error");
		return 0;
	}

	used = (int*)malloc(count(A)*sizeof(int));
	for (int i = 0; i < count(A); i++)
		used[i] = 0;

	godeep(0);
	printf("\n");
	gowidth();

    return 0;
}


