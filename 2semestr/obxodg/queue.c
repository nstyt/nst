#include "queue.h"

queue* push(queue* L, int v1) {
	if (L == NULL) {
		queue *H = (queue*)malloc(sizeof(queue));
		H->v = v1;
		H->next = NULL;
		L = H;
	}
	else {
		L->next = (queue*)malloc(sizeof(queue));
		L = L->next;
		L->v = v1;
		L->next = NULL;
	}
	return L;
}

queue* pop(queue*H, int *v1) {
	if (H == NULL) {
		printf("Queue isn't exist.\n");
		return 0;
	}
	*v1 = H->v;
	queue *T = H;
	H = H->next;
	free(T);
	
	return H;
}